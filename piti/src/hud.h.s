;; -----------------------------------------
;; HUD PUBLIC FUNCTIONS
;; -----------------------------------------
.globl hud_draw
.globl hud_update
.globl hud_changeValues
.globl hud_resetValues
.globl hud_checkValues

.globl hud_getHundreds
.globl hud_getTens
.globl hud_getUnits