.area _DATA

.area _CODE

.include "cpctelera.h.s"
.include "macros.h.s"
.include "entity.h.s"
.include "video.h.s"
    
.globl _sprite_bullet


bala bala1,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala2,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala3,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala4,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala5,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala6,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala7,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala8,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala9,  -1, -1, 2, 4, 0, _sprite_bullet
bala bala10, -1, -1, 2, 4, 0, _sprite_bullet



init_balas::

    ld ix, #bala1_data ;; indice de la primera bala
    ld c, 8(ix)        ;; cargamos el numero total de balas para recorrer el bucle
    
    loop_init:
    
    ld a,#0
    ld 7(ix),a        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
    
    ld de,#0x000C
    add ix,de      
    dec c 
    jr nz, loop_init

    ret 
;; ------------------------
;; INPUTS
;;      B: Creador de la bala
;; ------------------------
bala_buscarBala::
    ;; 0 => Bala del jugador
    ;; 1 => Bala del enemigo
    
    ld iy, #bala1_data ;; cargamos la primera bala en el indice
    ld c, 8(iy) ;; cargamos el numero de balas totales para el bucle
    bucle: 
    ld a, 7(iy) ;; cargamos si la bala esta activada o no(1-activada,0-desactivada)
    cp #1
    jr z, is_0_buscarBala ;; si da 0 no esta activada por lo tanto me quedo con la que he encontrado
        
        ld bala_id(iy), b ;; Actualizamos el valor del creador de la bala
        ld a, #1    
        ld 7(iy), a ;; activamos la bala
        ld a, Ent_dir(ix);;
        ld Ent_dir(iy),a;;
        cp #1
        jr z, is_0_direccion
        ;;----derecha-------
        ld a, Ent_x(ix)
        ld b,a
        ld a, #4
        add b
        ld Ent_x(iy),a
        ld a, Ent_y(ix)
        ld b, #6
        add b
        ld Ent_y(iy),a
        
        ret
        is_0_direccion:
        ;;----izquierda-------        
        ld a, Ent_x(ix)
        ld b,a
        ld a,#4
        add b
        ld Ent_x(iy),a
        ld a, Ent_y(ix)
        ld b, #6
        add b
        ld Ent_y(iy),a  


    ret

    is_0_buscarBala: ;; si no da 0 quiere decir que la bala esta activada, por lo tanto salto a la siguiente

        ;; incremetamos el indice para acceder a la siguiente posicion de la bala
        ld de,#0x000C
        add iy,de
        dec c

    jr nz, bucle

    ret


bala_Update::

    ld ix, #bala1_data ;; indice de la primera bala
    ld c, 8(ix)        ;; cargamos el numero total de balas para recorrer el bucle
    
    loop_update:
    
    ld a, 7(ix)        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
    cp #1
    jr nz, not_0_activarDisparoupdate ;;si la bala no esta activada saltamos a la siguiente


        push bc        ;; guardo bc porque la funcion draw los destruye
        
        call bala_update
                        ;; en esta linea falta poner una condicion para que no dibuje una vez-
                        ;; este descativada la bala, esto se produce en la funcion update
        
        pop bc

    not_0_activarDisparoupdate:: 

        ;; salto a la siguiente bala
        ld de,#0x000C
        add ix,de   
       
    dec c 
    jr nz, loop_update


    ret



;; -----------------------------------------
;; Draws the obstacle
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
bala_drawAll::

    ld ix, #bala1_data ;; indice de la primera bala
    ld c, 8(ix)        ;; cargamos el numero total de balas para recorrer el bucle
    
    loop_draw:
    
    ld a, 7(ix)        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
    cp #1
    jr nz, not_0_activarDisparodraw ;;si la bala no esta activada saltamos a la siguiente


    push bc        ;; guardo bc porque la funcion draw los destruye


    call video_getVideoPointer
    ex de,hl
    call draw       ;; Draw bala


    pop bc

    not_0_activarDisparodraw:: 

        ;; salto a la siguiente bala
        ld de,#0x000C
        add ix,de   
       
    dec c 
    jr nz, loop_draw


   ret

;;;; -----------------------------------------
;; Erases the obstacle
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
bala_eraseAll::


    ld ix, #bala1_data ;; indice de la primera bala
    ld c, 8(ix)        ;; cargamos el numero total de balas para recorrer el bucle
    
    loop_erase:
    
    ld a, 7(ix)        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
    cp #1
    jr nz, not_0_activarDisparoerase ;;si la bala no esta activada saltamos a la siguiente


    push bc        ;; guardo bc porque la funcion draw los destruye


    call video_getVideoPointer
    ex de,hl             
    call erase_sprite       ;; Clear our hero
    


    pop bc

    not_0_activarDisparoerase:: 

        ;; salto a la siguiente bala
        ld de,#0x000C
        add ix,de   
       
    dec c 
    jr nz, loop_erase

    ret

;;;; -----------------------------------------
;; Erases the obstacle
;;Inputs:IX
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
bala_erase::

    call video_getVideoPointer
    ex de,hl
    call erase_sprite
    
    call video_cambiarBuffers

    call video_getVideoPointer
    ex de,hl
    call erase_sprite
    
    call video_cambiarBuffers        

    ret


;; -----------------------------------------
;; Update bala
;; DESTROYS: AF, BC, DE, HL , IX
;; -----------------------------------------
  bala_update:
       ;; Move obstacle to the left

    ld a, Ent_x(ix)
    ld Ent_px(ix),a
    ld a, Ent_y(ix)
    ld Ent_py(ix),a    

    ld a, Ent_dir(ix)
    cp #0
    jr nz, disparo_izquierda

;;-----------Derecha--------------------------------------
   
    ld a, Ent_x(ix)               ;; |
    inc a
    ;;inc a
    cp #78                       ;; | Obs_x--
    jr nz, not_restart_x_d        ;; If (obs_x != 0) then not_restart_x
    
        ;; Restart x when it is 0
        
        
        ld a, #0           ;; A = Obs_x = #80-1 Restart x position
        ld 7(ix),a
        call bala_erase

    not_restart_x_d:
        ld Ent_x(ix), a          ;;Update obs_x

        ret  
;;-----------Izquieda-------------------------
    disparo_izquierda:
    ld a, Ent_x(ix)               ;; |
    dec a
    ;;dec a 
    cp #1                      ;; | Obs_x--
    jr nz, not_restart_x_i        ;; If (obs_x != 0) then not_restart_x
    
        ;; Restart x when it is 0
        
        ld a, #0          ;; A = Obs_x = #80-1 Restart x position
        ld 7(ix),a
        call bala_erase
       
    not_restart_x_i:
        ld Ent_x(ix), a          ;;Update obs_x

    ret


bala_GetPointerData::
    ld iy, #bala1_data
    ret 