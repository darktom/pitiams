.area _CODE

.include "cpctelera.h.s"

.globl _g_tileset
.globl _level1;;id1
.globl _level2;;id2
.globl _level3;;id3
.globl _end;; gameover

map_id: .db #1
actual_map: .dw #_level1

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE DATA
;; -----------------------------------------
;; -----------------------------------------

;; -----------------------------------------
;; -----------------------------------------
;; PUBLIC FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------



;;-----------------------------------------
;;Outputs:A
;;-----------------------------------------
map_GetmapID::

    ld a,(map_id)

    ret

map_initialize::

    ;; ----------------
    ;; LOADING TILESET
    ;; ----------------

    ld a,#1
    ld (map_id),a
    ld hl, #_g_tileset      ;; Get the tiles
    call cpct_etm_setTileset2x4_asm

    
    ld hl,#0x8000
    ld de,#0x8001
    ld (hl),#00
    ld bc, #0x4000
    ldir

    ret

;;------------------------------------------
;;Inputs:A
;;-----------------------------------------
map_SetmapID::

    ld (map_id),a

    ret
;; -----------------------------------------
;; Draws the map
;; DESTROYS: AF, BC, DE, HL AF’, BC’, DE’, HL’
;; -----------------------------------------
map_draw::
    ld hl, (actual_map)         ;; Tileset to load
    push hl                 ;; Tileset load from stack
    

    ld hl,#0x0140     ;; le sumo 140-Hex 320-DEC a la posicion del mapa segun su buffer
    add hl,de
    push hl
                     ;; Video memory load from stack
    ld  bc, #0X0000           ;; Starting point (x, y)
    ld  de, #0x2a28         ;; Tilemap 20x20
    ld  a, #40              ;; Tilemap width
    call cpct_etm_drawTileBox2x4_asm
    ret

;; -----------------------------------------
;; Function to modify the map
;; Input:
;;      HL => Map pointer to change
;; -----------------------------------------
map_change::
    
    ld a, (map_id)
    cp #1
    jr nz ,LVL_2_Or_3

        ;; lvl 1

        ld hl , #_level2
        ld (actual_map), hl
        ld a, #2
        ld (map_id),a
         jr continuar

    ;; lvl 2 o 3
    LVL_2_Or_3:
    cp #2
    jr nz, LVL_3 
         ;; lvl 2

        ld hl , #_level3
        ld (actual_map), hl
        ld a, #3
        ld (map_id),a
        jr continuar

    LVL_3:
         ;; lvl 3

        ld hl , #_level1
        ld (actual_map), hl
        ld a, #1
        ld (map_id),a
        jr continuar

    continuar:
    ret

;; -----------------------------------------
;; Function to return the pointer of the actual map
;; RETURNS:
;;      HL => Actual Map pointer
;;      A  => Map id
;; -----------------------------------------
map_getPointerMap::
    ld hl, (actual_map)
    ret


;; -----------------------------------------
;; Function to show black screen 
;; RETURNS:
;;      HL => Actual Map pointer
;;      
;; -----------------------------------------
map_gameOver::
    ld hl, #_end
    ld (actual_map), hl
    call map_initialize
    ld de,#0xC000
    call map_draw ;;dibujamos el hud en los dos buffers una vez
    ld de,#0x8000
    call map_draw
ret