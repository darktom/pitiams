.area _DATA
.area _CODE
    
.include "macros.h.s"
.include "cpctelera.h.s"
.include "map.h.s"
.include "hud.h.s"
.include "hero.h.s"
.include "bala.h.s"
.include "video.h.s"
.include "enemigo.h.s"

.globl _g_tileset

borrado_bala: .dw #0
axuborrado_bala: .dw #0

;; -----------------------------------------
;; Draw function
;; INPUTS:
;;      A => Colout Pattern
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
draw::

    ;;Calculate screen position
    ;;ld de, #0xc140                ;; Puntero a la memoria de video
    ld hl,#0x0140
    add hl,de
    ex de,hl
    ld c, Ent_x(ix)              ;; Cargamos en c, (hero_x)                 
    ld b, Ent_y(ix)              ;; Cargamos en b, (hero_y)
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    
    ex de, hl
    ld c, Ent_w(ix);; Cargamos en c la anchura 12 pixels 6 bytes 
    ld b, Ent_h(ix);; Cargamos en b la altura 24 pixels 18 en hexadecimal
 
    ld h, Ent_spr_h(ix)     ;; Cargamos en h el byte perteneciente al Sprite 
    ld l, Ent_spr_l(ix)     ;; Cargamos en l el byte perteneciente al Sprite
    call cpct_drawSpriteMasked_asm
    

    ret

;; --------------
;; INPUTS:
;;      IX => Datos de Jugador / Enemigo
;;      IY => Datos de la bala
collision::

    ld c, 8(iy)        ;; cargamos el numero total de balas para recorrer el bucle
    
    loop:
    
    ld a, 7(iy)        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
    cp #1
    jp nz, not_0_activarDisparo ;;si la bala no esta activada saltamos a la siguiente

        push bc ;; salvamos BC para poder guardar el contador del bucle
        ;; hero_x + hero_w <= bala_x
        ld a, Ent_x(iy)
        ld b,a          ;; B=BALA_X
        ld a, Ent_x(ix)
        ld c, a         ;; C=HERO_X
        ld a, Ent_w(ix) ;; A=HERO_W
        add c           ;; sumamos hero_x + hero_w
        sub b           ;; restamos bala_x
        jp z, no_colisiona
        jp m, no_colisiona

        ;;bala_x + bala_w <= hero_x

        ld a, Ent_x(iy) ;; 
        ld b, a       ;; B=BALA_X
        ld a, Ent_w(iy) ;; A=BALA_W
        add b       ;; sumamos bala_x + bala_W
        sub c       ;; restamos personaje eje x
        jp z, no_colisiona
        jp m, no_colisiona


        ;; como colisiona en el eje X comprobamos si tambien colisiona en el eje Y

        ;; hero_y + hero_h <= bala_y
        ld a , Ent_y(iy)
        ld b,a         ;; cargamos bala_y en b
        ld a , Ent_y(ix)
        ld c , a       ;; cargamos hero_y en c
        ld a , Ent_h(ix) ;; cargamos hero_h en a
        add c           ;; sumamos hero_y + hero_h
        sub b           ;; restamos bala_y al acomulador
        jr z, no_colisiona
        jp m, no_colisiona

        ;; bala_y + bala_h <= hero_y

        ld a, Ent_y(ix)  
        ld c,a
        ld a, Ent_h(iy)
        add b
        sub c
        jr z, no_colisiona
        jp m, no_colisiona
        
        ;; -------------
        ;; COLISIONA
        ;;Ent_id(ix)
        ;;bala_id(iy)
        ld a, Ent_id(ix)    ;; Identificamos el colisionado
        cp #0               ;; 0 - Player, 1 - Enemigo
        jr nz, es_enemigo    ;; If Colisionado == player
            
            ;; bala_id=0 => jugador
            ;; ES JUGADOR
            ;; Comprobamos si la bala es del enemigo
            ld a, bala_id(iy)
            cp #0           ;; IF Bala id == 0
            jr z, no_colisiona ;;  Bala jugador - jugador Colisionado
                
                ;; BALA ENEMIGO - JUGADOR COLISIONADO
                ;call hud_changeValues
                call hero_hit
                jr caso_nulo
                

            ;; ES ENEMIGO
            es_enemigo:
                ld a, bala_id(iy)
                cp #0
                jr nz, no_colisiona
                    
                    ;; BALA JUGADOR - ENEMIGO COLISIONADO
                    call hud_changeValues
                    ;; call hero_heal 
                    call enemigo_hit

        caso_nulo:
            
            ld a,#0 ;; como se produce colision ponemos la bala en inactivo y la guardamos en su variable 
            ld 7(iy),a
            ld (axuborrado_bala),ix
            ld (borrado_bala),iy
            ld ix,(borrado_bala)
            call bala_erase
            ld ix,(axuborrado_bala)
            pop bc ;; desapilamos bc para que no pete el programa aunq no lo vayamos a utilizar
        
        ret

            
        no_colisiona: ;; si no hay colision no hacemos nada desapilamos C para volver a obtener el valor del contador

        ;;ld a, #0x00
        ;;ld (0xC001),a
        ;;ld (0xC002),a
        pop bc 

    not_0_activarDisparo: 

        ;; salto a la siguiente bala
        ld de,#0x000C
        
        add iy,de

    dec c 
    jp nz, loop

    ret     


erase_sprite::
    ;ld hl, #_level1
    call map_getPointerMap
    push hl                 ;; Tileset load from stack
    ;;ld  hl, #0xc140          ;; Starting video memory
    ld hl,#0x0140
    add hl,de
    push hl                 ;; Video memory load from stack




    call posicion_tilemap_anterior
    call tiles_ancho_alto

    inc e
    inc d
    inc d

;;---------MapWidth----------------------

    ld a,#40    

    call  cpct_etm_drawTileBox2x4_asm
    
    ret



    collision_map::



    call posicion_tilemap
    call tiles_ancho_alto

    ;; C=x , B=Y, E=W, D=H

    ;;calculamos cuanto tenemos que sumarle a la posicion de memoria del mapa para encontrar la zona de abajo 
    ;;de los pies del personaje  (B + H) x 40 + C,40 es el ancho del array de tiles 
    
    ld a,b 
    add d
    ld b,a ;; contador para el bucle de la funcion mutiplicar B=B+H
   

    ld a,e ;; guardo e en el acomulador, lo usare mas tarde
    ld de,#40 ;;cargamos el valor 40 que es lo que ocupa 1 fila de ancho en el array del mapa

    call multiplicar

    add hl,bc ;; HL = (B+H)x 40 + C, esta seria la posicion de abajo izquierda del sprite

    ld e,a

        bucle1:

        ld a, (hl)
        cp #0

        jr nz, no_hay_colision

        ld a,#1
        ld 12(ix),a
        ld a,#2
        ld 13(ix),a

        ret 

        no_hay_colision:

        ld a,#0
        ld 12(ix),a
        

        inc hl
        dec e
        jr nz,bucle1

    ret


;; ---------------------------
;; Calcula posicion en tiles de los sprites en el mapa
;; ---------------------------
posicion_tilemap::

    ;;-----------C=X------------------------- posicion X del tile
    
    ld a, Ent_x(ix)
    ld d,#2
    ld e,#0
    call dividir
    ld c,e

;;---------B=Y---------------------------- posicion Y del tile

    ld a, Ent_y(ix)
    ld d,#4
    ld e,#0
    call dividir
    ld b,e

    ret

posicion_tilemap_anterior::

;;-----------C=X------------------------- posicion X del tile
    
    ld a, Ent_px(ix)
    ld d,#2
    ld e,#0
    call dividir
    ld c,e

;;---------B=Y---------------------------- posicion Y del tile

    ld a, Ent_py(ix)
    ld d,#4
    ld e,#0
    call dividir
    ld b,e


    ret


;; calcula tamaño en tiles de los sprites en el mapa

tiles_ancho_alto::


;;--------W=E---------------------------- Ancho en tiles
    ld a,Ent_w(ix)
    ld d,#2
    ld e,#0    
    call dividir


    push de

;;-------H=D----------------------------- Alto en tiles
    ld a,Ent_h(ix)
    ld d,#4
    ld e,#0
    call dividir
    ld a,e
    
    pop de

    ld d,a



    ret

;;
;; numero_par:
;;
;;
;;    par_loop:
;;        
;;        sub d       ;; Dividendo restamos divisor
;;        
;;        jr z, par
;;        jp m, impar    ;;si no da 0 ni negativo acumulamos resultado
;;        
;;        jr par_loop
;;        impar:
;;        ld e, #1
;;        ret 
;;
;;        par:
;;        ld e,#0
;;
;;    ret 


multiplicar::

    multiplicar_loop:
    
    add hl,de

    dec b
    jr nz, multiplicar_loop

    ret




dividir::
    
    division_loop:
        
        call es_negativo ;; comprobamos si es negativo para que no salga nada mas empezar el bucle

        sub d       ;; Dividendo restamos divisor

        jp m, nada    ;;si no da 0 ni negativo acumulamos resultado
        inc e
        jr division_loop
        nada:

    ret


;; 0 si el numero no es negativo y 1 si el numero es negativo
es_negativo:

    loop_negativo:
    
    cp #0
    jp m, sigue_negativo ;; si no es negativo me voy a la funcion division

    ret
    
    sigue_negativo: ;; si sigue siendo negativo le resto , e incremento e , hasta que se haga positivo

    sub d
    inc e

    jr loop_negativo

    ret


