.area _CODE

.include "cpctelera.h.s"
.include "hero.h.s"
.include "macros.h.s"
.include "map.h.s"

.globl _H_HUD_0
.globl _H_HUD_1

.globl _H_hearts_0
.globl _H_hearts_1
.globl _H_hearts_2

hud_text: .dw "SC", "OR", "E:", #0

;; ASCII codes for the score values
;; From 0 to 9 => 0x30 to 0x39
hud_scoreHund: .db #0x30
hud_scoreTens: .db #0x30
hud_scoreUnit: .db #0x30

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE DATA
;; -----------------------------------------
;; -----------------------------------------

;; -----------------------------------------
;; Check the hero data to draw the hearts
;; -----------------------------------------
hearts_toDraw:
    call hero_GetPointerDataIX    ;; IX = hero data
    ld a, Ent_vida(ix)          ;; Points to life

    ;; Check if its 3 hearts
    cp #3
    jr z, sprite_3
    ;; Check if its 2 hearts
    cp #2
    jr z, sprite_2
    ;; Check if its 1 hearts
    cp #1
    jr z, sprite_1

    ;; Points to the sprite with 3 heart
    sprite_3:
        ld hl, #_H_hearts_2
        ret

    ;; Points to the sprite with 2 heart
    sprite_2:
        ld hl, #_H_hearts_1
        ret
    
    ;; Points to the sprite with 1 heart
    sprite_1:
        ld hl, #_H_hearts_0
        ret
    

    ret

;; -----------------------------------------
;; -----------------------------------------
;; PUBLIC FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------

hud_getHundreds::
    ld a, (hud_scoreHund)
ret

hud_getTens::
    ld a, (hud_scoreTens)
ret

hud_getUnits::
    ld a, (hud_scoreUnit)
ret

;; -----------------------------------------
;; draw the HUD
;; INPUTS: DE
;; DESTROYS: AF, BC, DE, HL AF’, BC’, DE’, HL’
;; -----------------------------------------
hud_draw::


    ld hl, #_H_HUD_0
                                ;; Points to the video memory
    ld c, #40                   ;; Width image in bytes
    ld b, #32
    push de                   ;; Height image in pixels
    call cpct_drawSprite_asm
    pop de

    ;;ld de, #0xC000               ;; Points to the video memory
    
    ld bc, #0x0028
    push de
                   ;; POSITION (X, Y) => 9(40, 0)
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl
    ld hl, #_H_HUD_1
    ld c, #40                    ;; Width image in bytes
    ld b, #32                    ;; Height image in pixels
    call cpct_drawSprite_asm

    ;; ---------------------------
    ;; TEXT
    ;; ---------------------------

    ;;(2B DE) screen_start  Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x [0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y [0-199] row starting from 0 (y coordinate) in bytes)
    ;;ld de, #0xC000               ;; Points to the video memory
    ld c, #30                    ;; POS X
    ld b, #12                    ;; POS Y
    pop de
    call cpct_getScreenPtr_asm   ;; Returns to HL (Location)

    ex de, hl
    ;;(2B HL) string    Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory  Video memory location where the string will be drawn
    ;;(1B C ) fg_pen    Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen    Background colour (PEN, 0-15)
    ld hl, #hud_text
    ld c, #0x0F
    ld b, #0x01
    call cpct_drawStringM0_asm

    ret

;; -----------------------------------------
;; Updates the HUD
;; INPUTS: 
;;      DE: Points to the video memory
;; DESTROYS: AF, BC, DE, HL AF’, BC’, DE’, HL’
;; -----------------------------------------
hud_update::
    
    ;;(2B DE) screen_start  Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x [0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y [0-199] row starting from 0 (y coordinate) in bytes)
    push de                      ;; Video position
    ld c, #4                    ;; POS X
    ld b, #12                    ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to HL (Location)

    ex de, hl
    

    call hearts_toDraw          ;; Returns to HL Sprite to draw
    ;;ld hl, #_H_hearts_0
    ld c, #20                    ;; Width image in bytes
    ld b, #10                    ;; Height image in pixels
    call cpct_drawSprite_asm


    
    ;;(2B DE) screen_start  Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x [0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y [0-199] row starting from 0 (y coordinate) in bytes)
    pop de      ;; Video position
    ld c, #55                    ;; POS X
    ld b, #12                    ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to HL (Location)

    ex de, hl   ;; Change values between DE && HL

    

    ;; Draw the first number of the score
    push de     ;; Video position
    ld c, #0x0F
    ld b, #0x01
    ld a, (hud_scoreHund)
    call cpct_drawCharM0_asm

    ;; Calculate the position for the next number
    pop de      ;; Video position
    ld hl, #4   ;; Move to the right
    add hl, de  ;; Value saved to HL
    ex de, hl   ;; Change values between DE && HL
    
    

    ;; Draw the second number of the score
    push de     ;; Save DE for the next number
    ld c, #0x0F
    ld b, #0x01
    ld a, (hud_scoreTens)
    
    call cpct_drawCharM0_asm
    
    ;; Calculate the position for the next number
    pop de      ;; Video position
    ld hl, #4   ;; Move to the right
    add hl, de  ;; Value saved to HL
    ex de, hl   ;; Change values between DE && HL
    
    ;; Draw the third number of the score
    ld c, #0x0F
    ld b, #0x01
    ld a, (hud_scoreUnit)
    
    call cpct_drawCharM0_asm

    ret

;; -----------------------------------------
;; Function to modify the values of the HUD
;; INPUTS:
;; DESTROYS:
;; -----------------------------------------
hud_changeValues::

    ;; If unidades == 0x39
    ;;      If decenas == 0x39
    ;;          Centenas++
    ;;      else Decenas++
    ;; else Unidades++
    ld a, (hud_scoreUnit) ;; A = Score Units
    cp #0x39              ;; Check if is 9 value
    jr z, increment_tens   ;; If is 9, increment tens

        ;; Else => Units++
        inc a
        ld (hud_scoreUnit), a
        jr end_increment

    ;; Check if tens == 9
    increment_tens:
        ;;Reset unit values
        ld a, #0x30
        ld (hud_scoreUnit), a

        ld a, (hud_scoreTens) ;; A = Score Tens
        cp #0x39              ;; Check if is 9 value
        jr z, increment_hundreds   ;; If is 9, increment tens

            ;; Else => Tens++
            inc a
            ld (hud_scoreTens), a
            jr end_increment

        increment_hundreds:
            ;;Reset tens values
            ld a, #0x30
            ld (hud_scoreTens), a
            
            ld a, (hud_scoreHund) ;; A = Score Hundreds
            cp #0x39              ;; Check if is 9 value
            jr z, reset_score        ;; If is 9
                
                ;; Else => Hundreds++
                inc a
                ld (hud_scoreHund), a
                jr end_increment

            reset_score:
                ;;Reset tens values
                ld a, #0x30
                ld (hud_scoreHund), a

    end_increment:
    ret

;; -----------------------------------------
;; Function to reset the values of the HUD
;; INPUTS:
;; DESTROYS:
;; -----------------------------------------
hud_resetValues::
    ld a, #0x30
    ld (hud_scoreUnit), a
    ld (hud_scoreTens), a
    ld (hud_scoreHund), a
ret


;; -----------------------------------------
;; Function to check for the map change
;; INPUTS:
;; DESTROYS:
;; -----------------------------------------
hud_checkValues::

    ld a, (hud_scoreTens)
    cp #0x31
    ret nz

   ;; ld hl, #_level2
   ;; ld a, #2
    call map_change
    ld a, #3
   
ret



