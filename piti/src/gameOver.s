.area _DATA
.area _CODE

.include "cpctelera.h.s"
.include "keyboard.h.s"
.include "hud.h.s"

text_gameOver: .dw "GA", "ME", " O", "VE", "R ", #0
text_scoreOver: .dw "SC", "OR", "E:", #0
text_continue: .dw "PR", "ES", "S ", "AN", "Y ", "KE", "Y ", #0

;; -----------------------------------------
;; Draws the Game Over screen with a sprites and text (160x200)
;; INPUTS:
;;      DE => Points to the video memory
;; -----------------------------------------
gameOver_draw::
    ;;(2B DE) screen_start	Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x	[0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y	[0-199] row starting from 0 (y coordinate) in bytes)
    push de
    ld c, #20                    ;; POS X
    ld b, #80                    ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_gameOver
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm

    ;; -------------------------
    ;; CONTINUE TEXT
    ;; -------------------------
    
    ;;(2B DE) screen_start	Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x	[0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y	[0-199] row starting from 0 (y coordinate) in bytes)
    pop de
    push de
    ld c, #15                    ;; POS X
    ld b, #120                    ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_continue
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm
   
    ;; -------------------------
    ;; SCORE TEXT
    ;; -------------------------

    ;;(2B DE) screen_start	Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x	[0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y	[0-199] row starting from 0 (y coordinate) in bytes)
    pop de
    push de
    ld c, #20                    ;; POS X
    ld b, #100                    ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_scoreOver
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm

    ;; -------------------------
    ;; SCORE
    ;; -------------------------
    pop de
    ld c, #45                    ;; POS X
    ld b, #100                   ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl   ;; Change values between DE && HL

    ;; Draw the first number of the score
    push de     ;; Video position
    ld c, #0x0F
    ld b, #0x00
    call hud_getHundreds
    call cpct_drawCharM0_asm

    ;; Calculate the position for the next number
    pop de      ;; Video position
    ld hl, #4   ;; Move to the right
    add hl, de  ;; Value saved to HL
    ex de, hl   ;; Change values between DE && HL
    
    ;; Draw the second number of the score
    push de     ;; Save DE for the next number
    ld c, #0x0F
    ld b, #0x00
    call hud_getTens

    call cpct_drawCharM0_asm
    
    ;; Calculate the position for the next number
    pop de      ;; Video position
    ld hl, #4   ;; Move to the right
    add hl, de  ;; Value saved to HL
    ex de, hl   ;; Change values between DE && HL
    
    ;; Draw the third number of the score
    ld c, #0x0F
    ld b, #0x00
    call hud_getUnits
    call cpct_drawCharM0_asm

    ret

;; -----------------------------------------
;; Update function for Game Over screen
;; RETURN:
;;      C => GAME STATE
;; -----------------------------------------
gameOver_update::
    call cpct_scanKeyboard_if_asm

    call cpct_isAnyKeyPressed_asm
    cp #0                       ;; Check A == 0
    jr z, fueraa         ;; Jump if A == 0, move to the right

        ;; Clear the screen
        ld c, #0         ;;Back to title
        ret

    fueraa:
ret