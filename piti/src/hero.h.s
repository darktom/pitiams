;; -----------------------------------------
;; HERO PUBLIC FUNCTIONS
;; -----------------------------------------
.globl hero_draw
.globl hero_erase
.globl hero_update
.globl checkUserInputPush
.globl hero_GetPointerDataIX
.globl hero_GetPointerDataIY
.globl update_frame
.globl hero_heal
.globl hero_hit
.globl hero_getLifes

.globl hero_initV
.globl hero_initP