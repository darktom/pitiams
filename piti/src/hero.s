.area _DATA
.area _CODE
;; -----------------------------------------
;; CPCtelera Globals
;; -----------------------------------------
.include "hero.h.s"
.include "keyboard.h.s"
.include "cpctelera.h.s"
.include "macros.h.s"
.include "bala.h.s"
.include "entity.h.s"
.include "video.h.s"
.include "map.h.s"

.globl get_control

.globl _sprt_player_0
.globl _sprt_player_1 
.globl _sprt_player_2
.globl _sprt_player_3
.globl _sprt_player_4
.globl _sprt_player_5
.globl _sprt_player_6
.globl _sprt_player_7

actual_frame: .db #1
delay_frame:  .db #5
salto_frame : .db #1            ;; 1 == salto   0== no salto
delay_salto: .db #50



Posx: .db #20
Posy: .db #50
Vidas:.db #3    
;; -----------------------------------------
;; ARRAY DE SALTO PERSONAJE
;; -----------------------------------------
jumptable:
    .db #-20, #-5, #-5, #-3
    .db #-3, #-2, #-2, #-1
    .db #-1, #0x80 ;; Finish jumping flag

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE DATA
;; -----------------------------------------
;; -----------------------------------------
personaje hero, 20, 50, 8, 18, 3, 0, _sprt_player_0

;;personaje pene, 20, 120, #6, #22, 3, 0, _sprite_hero

;; -----------------------------------------
;; -----------------------------------------
;; PUBLIC FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------



;; -----------------------------------------
;; Resets the hero values
;; -----------------------------------------
hero_initV::
    
   
    ld a, (Vidas)
    ld (hero_v), a

    ret

hero_initP::

    ld a, (Posx)
    ld (hero_x), a

    ld a, (Posy)
    ld (hero_y), a      
    
    ret 

;; -----------------------------------------
;; Draws the hero
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
hero_draw::
    ld ix, #hero_data
    call video_getVideoPointer ;; cojo el puntero del buffer y se lo paso a draw en DE
    ex de,hl
    call draw

    ret

;; -----------------------------------------
;; Update the hero
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
hero_update::
    
    ld a, (hero_x)
    ld (hero_px),a
    ld a,(hero_y)
    ld (hero_py),a 

    call fallHero
    call jumpControl        ;; Do jumps n control
    ;; si no hace nada, seguira el sprite de parado
    ;;call checkUserInputPush
    call checkUserInputMove

        ld ix,#hero_data
        call map_getPointerMap
        call collision_map  

        
        ld ix,#hero_data
        call bala_GetPointerData
        call collision


    
    ret

;; -----------------------------------------
;; Function to decrement hero life
;; -----------------------------------------
hero_hit::
    ;(1B L ) sfx_num	Number of the instrument in the SFX Song (>0), same as the number given to the instrument in Arkos Tracker.
    ;(1B H ) volume	Volume [0-15], 0 = off, 15 = maximum volume.
    ;(1B E ) note	Note to be played with the given instrument [0-143]
    ;(1B D ) speed	Speed (0 = As original, [1-255] = new Speed (1 is fastest))
    ;(2B BC) inverted_pitch	Inverted Pitch (-0xFFFF -> 0xFFFF).  0 is no pitch.  The higher the pitch, the lower the sound.
    ;(1B A ) channel_bitmask	Bitmask representing channels to use for reproducing the sound (Ch.A = 001 (1), Ch.B = 010 (2), Ch.C = 100 (4))
    ld l, #1
    ld h, #15
    ld e, #50
    ld d, #1
    ld bc, #0x0000
    ld a, #100
    call cpct_akp_SFXPlay_asm

    ;; IF life > 0 => life--
    ld a, (hero_v)      ;; A = Vida del heroe
    cp #1               ;; Comprobar si es > 0
    jr z, Game_Over     ;; Caso de GAME OVER

        ;; Aun le queda vida
        dec a           ;; Se le resta vida
        ld (hero_v), a  ;; Actualizamos variable
        ret             ;; Volvemos al ruedo

    Game_Over:
        ld a, #0
        ld (hero_v), a
    ret

;; -----------------------------------------
;; Function to get the lifes of the hero
;; RETURN:
;;      A => lifes
;; -----------------------------------------
hero_getLifes::
    ld a, (hero_v)
    ret

;; -----------------------------------------
;; Function to increment hero life
;; -----------------------------------------
hero_heal::
    ;; IF life < 3 => life++
    ld a, (hero_v)      ;; A = Vida del heroe
    cp #3               ;; Comprobar si es < 3
    jr z, no_Heal       ;; No podemos sanar mas de 3 vidas

        ;; Tiene vida para curar
        inc a           ;; Se le suma vida
        ld (hero_v), a  ;; Actualizamos variable
        ret             ;; Volvemos al ruedo

    no_Heal:
    ret

;; -----------------------------------------
;; Erases the hero
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
hero_erase::
    ;; Calculate screen position
    
    
    ld ix, #hero_data
    call video_getVideoPointer
    ex de,hl
    call erase_sprite

    
    ret


;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------

;; -----------------------------------------
;; Flip the hero to look to the right
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
flipToRight:
    ld a, (hero_dir)   ;;Actual looking side 
    ;; IF a != 0? FLIP && dir=1
    cp #0
    jr z, not_flip_right

        ;; a = Hero_direction == 1
        ;; Flip to the right
        call flip
        ld a, #0                ;; Change value for the hero_direction
        ld (hero_dir), a  ;; Update hero_direction value

    not_flip_right:
    ret

;; -----------------------------------------
;; Flip the hero to look to the left
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
flipToLeft:
    ld a, (hero_dir)   ;;Actual looking side 
    ;; IF a != 0? FLIP && dir=0
    cp #1
    jr z, not_flip_left

        ;; a = Hero_direction == 0
        ;; Flip to the right
        call flip
        ld a, #1                ;; Change value for the hero_direction
        ld (hero_dir), a  ;; Update hero_direction value

   not_flip_left:
    ret

;; -----------------------------------------
;; Check user input function
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
checkUserInputMove:
    ;; Scan the whole keyboard 
    call cpct_scanKeyboard_if_asm  ;; No input values   

    ld a,#0
    ld (hero_mov),a
    ;; Check for pressed buttons
    ;; ----------
    ;; BUTTON D
    call get_control            ;; return in A: 0 for keyboard, 1 for joystick
    cp #0
    jr z,  keyboard_D
    jp nz, joystick_D
        ;; else es un 1 y es el joystcik
    joystick_D:
        ld hl, #Joy1_Right
        jr continua0
    keyboard_D:
        ld hl, #Key_D               ;; HL = Key_D keycode

   
    continua0:
    call cpct_isKeyPressed_asm  ;; Check if Key_D is pressed
    cp #0                       ;; Check A == 0
    jr z, d_not_pressed         ;; Jump if A == 0, move to the right

        ;; D is pressed
        ld a,#1
        ld (hero_mov),a
        call flipToRight
        call update_frame
        call moveHeroRight


        

    d_not_pressed:
    
    ;; ----------
    ;; BUTTON A OR left
    call get_control            ;; return in A: 0 for keyboard, 1 for joystick
    cp #0
    jr z,  keyboard_I
    jp nz, joystick_I
        ;; else es un 1 y es el joystcik
    joystick_I:
        ld hl, #Joy1_Left
        jr continua1
    keyboard_I:

        ld hl, #Key_A               ;; HL = Key_A keycode
   
    continua1:
    
    call cpct_isKeyPressed_asm  ;; Check if Key_A is pressed
    cp #0                       ;; Check A == 0
    jr z, a_not_pressed

        ;; A is pressed
            ;; si la direccion del personaje es 0, esta a la derecha, por lo que debemos voltear

            ld a,#1
            ld (hero_mov),a
            call flipToLeft
            call update_frame
            call moveHeroLeft



    a_not_pressed:

    ret


;; -----------------------------------------
;; Check user input push
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
checkUserInputPush::

    call cpct_scanKeyboard_if_asm
    ;; ----------
    ;; BUTTON W Or FIRE1
        call get_control            ;; return in A: 0 for keyboard, 1 for joystick
    cp #0
    jr z,  keyboard_W
    jp nz, joystick_W
        ;; else es un 1 y es el joystcik
    joystick_W:
        ld hl, #Joy0_Fire1          ;; HL == Fire1 keycode
        jr continua2
    keyboard_W:

        ld hl, #Key_W               ;; HL = W keycode
   
    continua2:
                   
    call cpct_isKeyPressed_asm  ;; Check if Key_W is pressed
    cp #0                       ;; Check W == 0
    jr z, w_not_pressed

        ;; W is pressed
        ld b, #0    ;; 0 => Bala del jugador
        ld ix, #hero_data
        call bala_buscarBala     
       
        

         ld a, (salto_frame)                ;;   
         cp #0                               ;;
         jr z, cargar_sprite                   ;; a !=0 scnd_frame
    
        ;; si ya esta saltando 
        jr continua_salto


        cargar_sprite:         
        ld hl, #_sprt_player_7               ;; cambiamos el sprite a disparo
           ld (hero_spr), hl
           ld a, #1                        ;; actual_frame == 2 para que llame a second frame
           ld (salto_frame), a
            ld a, (delay_salto)                 ;;\
            dec a                               ;; |
             ld (delay_salto) , a                            ;; |Decrementamos el contador de  frames para el cual se cambia de sprite
            ret nz                              ;;/

    ld a, #5                            ;;
    ld (delay_salto), a                 ;;Se vuelve a poner como estaba
           jr continua_salto_2


  
       
      


    w_not_pressed:

    continua_salto:
        ld hl, #_sprt_player_0
        ld (hero_spr), hl
        ld a, #0
        ld (salto_frame), a

    continua_salto_2:

    ;; ----------
    ;; BUTTON Space or fire2
          call get_control            ;; return in A: 0 for keyboard, 1 for joystick
    cp #0
    jr z,  keyboard_F
    jp nz, joystick_F
        ;; else es un 1 y es el joystcik
    joystick_F:
        ld hl, #Joy0_Fire2          ;; HL == Fire2 keycode
        jr continua3
    keyboard_F:

        ld hl, #Key_Space               ;; HL = Key_Space keycode
   
    continua3:
    call cpct_isKeyPressed_asm  ;; Check if Key_Space is pressed
    cp #0                       ;; Check Space == 0
    jr z, space_not_pressed

        ld a,#1
        ld (hero_mov),a
        ;; Space is pressed
      
        ld a, (hero_y)                      ;; A = Hero_y
          cp #30                            ;; que no salte si se pasa de la posicion 32 (HUD)
         
            jr z, continuar_sin_saltar
            jp m, continuar_sin_saltar
                ;; si no es negativo ni 0
               ld a,(hero_nSaltos) ;; cargo variable con numero de saltos, por defecto 0
                call startJump
            ;; 0 o negativo no salta
            continuar_sin_saltar:
        

    space_not_pressed:



    ret

;; -----------------------------------------
;; Controls jump movements
;; DESTROYS: AF,BC,HL
;; -----------------------------------------
jumpControl:

    ;; Check if we are jumping right now
    ld a, (hero_jump)       ;; A = Hero_jump status
    cp #-1                  ;; A == -1? (-1 is not jumping)
    ret z                   ;; If A==-1, not jumping, return
        ld hl, #_sprt_player_3
        ld (hero_spr), hl
    ;; Get jump value
    ld hl, #jumptable       ;; HL Point to start of the jumping table
    ld c, a                 ;; |
    ld b, #0                ;; \ BC = A (offset)
    add hl, bc              ;; HL += BC

    ;; Check end of jumping
    ld a, (hl)              ;; A = Jump movement
    cp #0x80                ;; Jump value == 0x80? End of jumping
    jr z, end_of_jump       ;; If 0x80, end of jump

    ;; Do jump movement
    ld b, a                 ;; B = A = Jump movement
    ld a, (hero_y)          ;; A = Hero_y
    add b                   ;; A += B (Add jump movement)
    ld (hero_y), a          ;; Update hero_y value
    ld hl, #_sprt_player_3                  ;;|
    ld (hero_spr), hl                   ;;\cuando no detecta colision , se activa el sprite del salto
    ;; Increment hero_jump index
    ld a, (hero_jump)       ;; A = Hero_jump
    inc a                   ;; |
    ld (hero_jump), a       ;; \ Hero jump ++
   

    ret

    ;; Put -1 in the jump index when jump ends
    end_of_jump:
        ld a, #-1           ;; |
        ld (hero_jump), a   ;; \ Hero_jump = -1
        ld hl, #_sprt_player_0
        ld (hero_spr), hl
      
       
    ret
   
;; -----------------------------------------
;; Starts hero jump
;; DESTROYS: AF
;; -----------------------------------------
startJump:

    ld a,(hero_nSaltos) ;; cargo variable con numero de saltos, por defecto 0
    cp #0
    jr z,no_salto
    
    dec a
    ld (hero_nSaltos),a
    ld a, (hero_jump)       ;; A = Hero_jump
    cp #-1                  ;; A == -1? Is jump active?  
    ret nz                  ;; Jump active, return

    ;; Jump is inactive, activate it
    ld a, #0
    ld (hero_jump), a
 
    no_salto:

    
    ret


;; -----------------------------------------
;; Move hero left if it is not at the limit
;; DESTROYS: AF
;; -----------------------------------------
moveHeroLeft:
    ld a, (hero_x)              ;; A = Hero_x
    cp #1                       ;; Check left limit
    jr z, not_move_l            ;; Hero_x == limit, do not move left
    
        ;; Move left
        dec a                  ;; A-- (Hero_x--)
        ld (hero_x), a          ;; Update Hero_x from A
    
    not_move_l:
    
    ret


;; -----------------------------------------
;; Move hero right if it is not at the limit
;; DESTROYS: AF
;; -----------------------------------------
moveHeroRight:
    ld a, (hero_x)              ;; A = Hero_x
    ;; 80 (pantalla) - 4 (borde) - 5 (ancho heroe)
    cp #71                      ;; Check right limit (screen width - hero_width)
    jr z, not_move_r            ;; Hero_x== limit, do not move right
    
        ;; Move right
        inc a                       ;; A++ (Hero_x++)
        ld (hero_x), a              ;; Update Hero_x from A
       
    
    not_move_r:
    
    ret



;; -----------------------------------------
;; Get pointer
;; DESTROYS: IX
;; -----------------------------------------
hero_GetPointerDataIX::
    ld ix ,#hero_data
    ret

hero_GetPointerDataIY::
    ld iy ,#hero_data
    ret
    
;; -----------------------------------------
;; fall hero
;; DESTROYS: AF
;; -----------------------------------------
fallHero:

    ld a, (hero_colS)
    cp #1
    jr nz, no_colisiona
    ;; colisiona
   ;; ld hl, #_sprt_player_0                  ;;| 
   ;; ld (hero_spr), hl                       ;;\ Cuando  detecta colision , se desactiva el sprite del salto
    ret 
   
    
    no_colisiona:

    ld a, (hero_y)
    inc a
    inc a
    inc a
    ld (hero_y),a
   ;; ld hl, #_sprt_player_3                  ;;|
   ;;     ld (hero_spr), hl                   ;;\cuando no detecta colision , se activa el sprite del salto
   
    ret

;; -----------------------------------------
;; Updates the frame for the next animation frame
;; input: 2 frames in stack, E = frameDelay
;; DESTROYS: HL, AF
;; -----------------------------------------
update_frame:
 
   call delay
    ld a, (actual_frame)                ;;   
    cp #1                               ;;
    jr nz, scnd_frame                   ;; a !=0 scnd_frame
    
    frst_frame:         
        ld hl, #_sprt_player_1
                                        ;; sacamos de la pila el primer sprite
        ld (hero_spr), hl
        ld a, #2                        ;; actual_frame == 2 para que llame a second frame
        ld (actual_frame), a

    ;;    ld a, (hero_dir)
    ;;    cp #1
    ;;    jr z, flip_frame
        
  

        ret

    scnd_frame:
        ld hl, #_sprt_player_2
                   
        ld (hero_spr), hl
        ld a, #1
        ld (actual_frame), a

    ;;    ld a, (hero_dir)
    ;;    cp #1
    ;;    jr z, flip_frame
 

        ret
    ;;flip_frame:
    ;;    ld hl, (hero_spr)    ;; HL points to the sprite
    ;;    ld a, (hero_h)
    ;;    ld b, a
    ;;    ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
    ;;    ld c, a
    ;;    call cpct_hflipSpriteMaskedM0_asm
    ;;    ret

;;=============================================
;;delay betwen sprites
;;=============================================
delay:
 ld a, (delay_frame)                 ;;\
    dec a                               ;; |
    ld (delay_frame) , a                            ;; |Decrementamos el contador de  frames para el cual se cambia de sprite
    ret nz                              ;;/

    ld a, #5                            ;;
    ld (delay_frame), a                 ;;Se vuelve a poner como estaba
ret
;;==============================================
;;Flip the character
;;==============================================
flip:
        ld hl, #_sprt_player_0    ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_1     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_2     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_3     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_4     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_5     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm

                ld hl, #_sprt_player_6     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm
         ld hl, #_sprt_player_7     ;; HL points to the sprite
        ld a, (hero_h)
        ld b, a
        ld a, (hero_w)          ;; Size of the sprite (width in bytes, height in pixels)
        ld c, a
        call cpct_hflipSpriteMaskedM0_asm
    ret