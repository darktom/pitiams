.area _DATA
.area _CODE
    
.include "macros.h.s"
.include "cpctelera.h.s"
.include "keyboard.h.s"

.globl _sprite_inicio_0
.globl _sprite_inicio_1

text_start:    .dw "SP", "AC", "E ", "TO", " S", "TA", "RT", #0
text_control:  .dw "MO", "VE", " W", "IT", "H ", "WA", "SD", " A", "ND", " S", "PA", "CE", #0
state_control: .db #0            ;;Estado para saber si es Keyboard (0) o joystick (1) Default (0)
text_selection:.dw "CH","OO","SE", " T","O ","ST","AR","T ",#0
text_keyboard: .dw "(Y",") ","KE","YB","OA","RD", #0
text_joystick: .dw "(U",") ","JO","YS","TI","CK", #0
;;nyapa: .dw "(Y",") ","KE","YB","OA","RD","  ","  ","  ","  ","  ","  ","  "," ,","  ","  ","  ","  ","  ","  ","(U",") ","JO","YS","TI","CK", #0
;;"PL","AY"," W","IT","H ",
;; -----------------------------------------
;; -----------------------------------------
;; PUBLIC FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------

;; -----------------------------------------
;; Draws the title screen with a sprites and text (160x200)
;; INPUTS:
;;      DE => Points to the video memory
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
init_draw::
    ld hl, #_sprite_inicio_0
    push de                     ;; Points to the video memory
    ld c, #40                   ;; Width image in bytes
    ld b, #80                   ;; Height image in pixels
    call cpct_drawSprite_asm
    
    pop de                       ;; Points to the video memory
    ld bc, #0x0028               ;; POSITION (X, Y) => 9(40, 0)
    push de                      ;; Saves the pointer to the memory
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl                    ;; Points to the video memory
    ld hl, #_sprite_inicio_1
    ld c, #40                    ;; Width image in bytes
    ld b, #80                    ;; Height image in pixels
    call cpct_drawSprite_asm

    ;; ------------------------
    ;; STRING
    ;; ------------------------


    ;;(2B DE) screen_start	Pointer to the start of the screen (or a backbuffer)
    ;;(1B C ) x	[0-79] Byte-aligned column starting from 0 (x coordinate,
    ;;(1B B ) y	[0-199] row starting from 0 (y coordinate) in bytes)
    pop de                       ;; Points to the video memory
    ld c, #10                    ;; POS X
    ld b, #80                   ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)
    push de
    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_selection
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm

    ;; texto TECLADO

    pop de                       ;; Points to the video memory
    ld c, #05                    ;; POS X
    ld b, #100                   ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)
    push de
    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_keyboard
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm
    ;; texto joystick

    pop de                       ;; Points to the video memory
    ld c, #00                    ;; POS X
    ld b, #120                   ;; POS Y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)
    ;;push de
    ex de, hl

    ;;(2B HL) string	Pointer to the null terminated string being drawn
    ;;(2B DE) video_memory	Video memory location where the string will be drawn
    ;;(1B C ) fg_pen	Foreground colour (PEN, 0-15)
    ;;(1B B ) bg_pen	Background colour (PEN, 0-15)
    ld hl, #text_joystick
    ld c, #0x0F
    ld b, #00
    call cpct_drawStringM0_asm

   
    ret

;; -----------------------------------------
;; Update function for title screen
;; INPUTS:
;; DESTROYS: AF, BC, DE, HL
;; RETURN:
;;      C => GAME STATE
;; -----------------------------------------
init_update::

    call cpct_scanKeyboard_if_asm


        ;; ----------
        ;; BUTTON U
        ld hl, #Key_U               ;; HL = Key_D keycode
        call cpct_isKeyPressed_asm  ;; Check if Key_D is pressed
        cp #0                       ;; Check A == 0
        jr z, u_not_pressed         ;; Jump if A == 0, move to the right

        ;; u  is pressed
         ld a, #1
            ld (state_control), a           ;;state control = 1 joys
            ld c, #0                        ;; (load game)
            ret

        u_not_pressed:

            ;; ----------
            ;; BUTTON y
            ld hl, #Key_Y               ;; HL = Key_D keycode
            call cpct_isKeyPressed_asm  ;; Check if Key_D is pressed
            cp #0                       ;; Check A == 0
            jr z, y_not_pressed         ;; Jump if A == 0, move to the right

            ;; y  is pressed
            ld a, #0                        ;; state control = 0 keyboard
            ld (state_control), a
            
            ld c, #0 ;; (load game)
            ret

            y_not_pressed:

            ret



        ret

    ret
;;------------------------------------------
;;get the control_State
;;------------------------------------------
get_control::

    ld a, (state_control)

    ret

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------

;;===================================================
;;draw_selection
;;===================================================
draw_selection:

        ld c, #60                    ;; POS X
        ld b, #85                    ;; POS Y
        call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

        ex de, hl
        ld hl, #text_keyboard
        ld c, #0x0F
        ld b, #00
        call cpct_drawStringM0_asm


        
        ex de, hl
        ld hl, #text_joystick
        ld c, #0xFF
        ld b, #00
        call cpct_drawStringM0_asm


ret

