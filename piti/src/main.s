.area _DATA
.area _CODE

.include "bala.h.s"
.include "hero.h.s"
.include "cpctelera.h.s"
.include "map.h.s"
.include "suelo.h.s"
.include "entity.h.s"
.include "enemigo.h.s"
.include "inicio.h.s"
.include "hud.h.s"
.include "video.h.s"
.include "gameOver.h.s"

.globl _sprt_palette
.globl _g_tileset


.macro Border color ;; consumo funciones

    ;;ld hl, #0x'color'10
    ;;call cpct_setPALColour_asm

.endm    


;;---------------------------
;;MUSIC
;;---------------------------
 .globl _song_ingame

update: .db #32 ;;llamamos 1 de cada 12 interrupciones al update de teclado (1 vez cada 6 fotogramas)
pantalla_titulo: .db #0
jugador: .db #0
interrupciones: .db #0x00
continterrupciones: .db #0
framerate: .db #22

contador_interrupciones::

    ld a,(continterrupciones)
    inc a
    ld (continterrupciones),a

    ret

establecer_framerate::

    ld a,(continterrupciones)
    cp #18
    jr z, continuo_main

    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt
    ;;halt

    continuo_main:

    ret


Change_level::

    call enemigo_Getnumactivo
    cp #0
    jr nz, no_cambio_nivel

    call map_change
    ld de,#0xC000
    call map_draw ;;dibujamos el hud en los dos buffers una vez
    ld de,#0x8000
    call map_draw

    call Reset_Personajes
 
    no_cambio_nivel:

    ret


Reset_Personajes::

    call init_balas
    call init_enemis
    call hero_initP

 ret    

isr_update:
     
    ld a, (pantalla_titulo)
    cp #0
    jr z, corta

        call contador_interrupciones

        ld a,(update) ;; cargamos el 36 
        dec a         ;; decrementamos
        ld (update), a ;; guardamos en update
        ret nz         ;; si a no es 0 encontes hacemos ret y salimos
        
        ;;call cpct_scanKeyboard_if_asm
         
        call checkUserInputPush

        ld a, #32  ;;reiniciamos la variable update para la siguiente interrupcion
        ld (update),a
   

    corta:
;;        
;;        ld l, #16
;;        ld a,(interrupciones)
;;        ld h,a
;;        inc a
;;        call cpct_setPALColour_asm
;;
;;
;;        ld a,(interrupciones)
;;        inc a
;;        cp #0x16
;;        jr nz, continue 
;;
;;        ld a,#0x00
;;    
;;     continue:
       
;;        ld (interrupciones),a

    ret
   
;; -----------------------------------------
;; Init functions to prepare the game
;; -----------------------------------------
init_program:
    call cpct_disableFirmware_asm

    ;; Set the video mode in 0
    ld c, #0
    call cpct_setVideoMode_asm

    ;; Change colour palette 
    ld hl, #_sprt_palette
    ld de, #16
    call cpct_setPalette_asm
    
    
    ;ld hl, #0x1410              ;; Set the border colour (16) into black
    ld hl, #0x0410              ;; Set the border colour (16) into black
    call cpct_setPALColour_asm  ;; Updates the border colour

    ret

;; -----------------------------------------
;; Main program
;; -----------------------------------------
_main::

    ld sp, #0x8000 ;;cambiamos la pila de posicion de memoria

    call init_program

    ;;---------------------------
    ;;MUSIC
    ;;---------------------------

    ld de, #_song_ingame
    call cpct_akp_musicInit_asm
    ld de, #_song_ingame
    call cpct_akp_SFXInit_asm
    ld hl, #isr_update
    call cpct_setInterruptHandler_asm


    title_screen:

    call cpct_akp_stop_asm
    ld a, #0
    ld (pantalla_titulo), a
    
    call map_gameOver

    ;; -------------
    ;; TITLE SCREEN
    ld de, #0xC000      ;; Points to the video memory
    call init_draw      ;; Draw the title screen
    ld de, #0x8000      ;; Points to the video memory
    call init_draw      ;; Draw the title screen

    title_loop:
        call init_update    ;; Check for user inputs
        ld a, c             ;; A = C = Title screen status
        cp #0               ;; Title screen == 0 => LOAD GAME        
    jr nz, title_loop

    ;; SET ISR 
    ld a, #1
    ld (pantalla_titulo), a

    ld hl, #isr_update
    call cpct_setInterruptHandler_asm


    call hero_initV
    call Reset_Personajes
   ;; ld hl, #_level1
   ;; ld a, #1
    ;;call map_change

    ld a, #1
    call map_SetmapID
    call map_change

        call hud_resetValues
        call map_initialize

        ld de,#0xC000
        call map_draw ;;dibujamos el hud en los dos buffers una vez
        ld de,#0x8000
        call map_draw
   
        ld de,#0xC000
        call hud_draw ;;dibujamos el hud en los dos buffers una vez
        ld de,#0x8000
        call hud_draw
    
 

    ;;ld hl, #jugador
    ;;call cpct_setInterruptHandler_asm
    
    game_loop:
        

        call Change_level

        call video_getVideoPointer ;; cojo el puntero de memoria para saber en que buffer estoy HL
        ex de,hl
        call hud_update     
        

        Border 1
        ;;---------------------------
        ;; ERASE
        ;;---------------------------
        call hero_erase     
        call enemigo_eraseAll
        call bala_eraseAll

        Border 2
        ;;---------------------------
        ;; UPDATE
        ;;---------------------------
        call hero_update        
        call bala_Update ;;  comprueba las balas activas las borra,actualiza y dibuja
        call enemigo_Update


        Border 1 
        ;;---------------------------
        ;; DRAW        
        ;;---------------------------
            
        call enemigo_drawAll
        call bala_drawAll
        call hero_draw 
        
        ;;---------------------------
        ;;VSYNC                
        ;;---------------------------
        
        call cpct_waitVSYNC_asm ;; Wait for the raster
        call video_cambiarBuffers

        ;; call cpct_akp_musicPlay_asm
        ;;call cpct_akp_musicPlay_asm
        call cpct_akp_musicPlay_asm

        ;;Comprobamos si el jugador tiene vidas
        call hero_getLifes  ;; A = Vidas del heroe
        cp #0               ;; Comprobamos si esta a 0
        jp z, gameOver_screen  ;; Volvemos a la pantalla de titulo si es 0

        call establecer_framerate

        ld a,#0
        ld (continterrupciones),a

    jr game_loop

    gameOver_screen:
        call map_gameOver
        call cpct_akp_stop_asm
        ld a, #0
        ld (pantalla_titulo), a

        ld de,#0x8000
        call gameOver_draw
        ld de,#0xC000
        call gameOver_draw

        gameOver_loop:
            call gameOver_update    ;; Check for user inputs
            ld a, c             ;; A = C = Title screen status
            cp #0               ;; Title screen == 0 => LOAD GAME        
        jp nz, gameOver_loop  ;; Volvemos a la pantalla de titulo si es 0
    
    jp title_screen
;; alias do="make && cpct_winape -a"
;; alias redo="make cleanall && make && cpct_winape -a"