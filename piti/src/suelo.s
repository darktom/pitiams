.area _DATA

.area _CODE
.include "cpctelera.h.s"
.include "macros.h.s"

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE DATA
;; -----------------------------------------
;; -----------------------------------------
suelo_x: .db #0
suelo_y: .db #191
suelo_w: .db #64
suelo_h: .db #10



;; Draws the obstacle
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
suelo_draw::
    ld a, #0xF0             ;; Yellow obstacle
    call drawSuelo       ;; Draw our obstacle
    ret


drawSuelo:

    push af                      ;; Save A in the stack

    ;; Calculate screen position
    ld de, #0xC000               ;; Video memory pointer
    ld a, (suelo_x)                ;; Cannot do ld c,(hero_x)
    ld c, a                      ;; C = obs_x
    ld a, (suelo_y)                ;; |
    ld b, a                      ;; \ B = obs_y
    call cpct_getScreenPtr_asm   ;; Returns to DE (Location)

    ex de, hl               ;; Exange HL && DE (Location)

    ;; Parameters for the solid box
    
                      ;; Colour from input
    ld a, (suelo_h)
    ld b ,a 
    ld a, (suelo_w)          ;; 4x4 pixeles
    ld c, a 
    pop af
    call cpct_drawSolidBox_asm
    
    ret




;; Colision suelo
;; DESTROYS: IX,A,B,C 
;; -----------------------------------------
suelo_Colision::

;; colision por el suelo desde arriba
;; hero_y + hero_height - suelo_y = 0
    ld a, (suelo_y)
    ld b,a 
    ld a, Ent_y(ix)
    ld c,a
    ld a, Ent_h(ix)
    add c ;; sumamos c al acomulador A= hero_y + hero_height
    sub b ;; restamos b al acomuladora A = (hero_y + hero_height) - suelo_y
    jr z, no_colision 
    jp p, no_colision 
 
 	ld 12(ix),#1
 	ld 13(ix),#0
 	ld a, #0xFF
 	ld (0xC000),a

    ret 
    no_colision:

    	ld 12(ix),#0
    	ld a, #0x00
    	ld (0xC000),a
    ret      	