;; -----------------------------------------
;; MACROS
;; -----------------------------------------

;; -----------------------------------------
;; MACRO PJ Y NPCS
;; -----------------------------------------

;;------------------------------PJ-----------------------------------------------
.macro personaje nombre, pos_x, pos_y, ancho, alto, vida, direccion, sprite

nombre'_data:

	nombre'_x: 	.db pos_x	;;0
	nombre'_y: 	.db pos_y	;;1
	nombre'_w: 	.db ancho	;;2
	nombre'_h: 	.db alto	;;3
	nombre'_dir:	.db direccion	;;4 direccion de sprite
	nombre'_spr: 	.dw sprite 	;;5-6 sprite en memoria
	nombre'_v:	.db vida	;;7 vida del pj
	nombre'_jump:	.db #-1		;;8 variable para activar el salto
	nombre'_px: 	.db pos_x	;;9 posicion previa para borrar X       
	nombre'_py:	.db pos_y	;;10 posicion previa para borrar y
	nombre'_ndisp:  .db #0		;;11 numero de disparos	 
	nombre'_colS: 	.db #0        	;;12 comprueba si colisona contra el suelo (0-no,1-si)
	nombre'_nSaltos: .db #0		;;13 comprueba el doble salto
	nombre'_id:     .db #0         	;;14
	nombre'_mov:	.db #0		;;15 el pj se mueve	      
	 
.endm


;;------------------------------ENEMIGO-----------------------------------------------
.macro enemigo nombre, pos_x, pos_y, ancho, alto, vida, direccion, sprite, contvision, coldowndisp, contdirection, semillaXor

nombre'_data:

	nombre'_x: 	.db pos_x		;;0
	nombre'_y: 	.db pos_y		;;1
	nombre'_w: 	.db ancho		;;2
	nombre'_h: 	.db alto		;;3
	nombre'_dir:	.db direccion		;;4 direccion de sprite
	nombre'_spr: 	.dw sprite 		;;5-6 sprite en memoria
	nombre'_v:	.db vida		;;7 vida del pj
	nombre'_jump:	.db #-1			;;8 variable para activar el salto
	nombre'_px: 	.db pos_x		;;9 posicion previa para borrar X       
	nombre'_py:	.db pos_y		;;10 posicion previa para borrar y
	nombre'_ndisp:  .db coldowndisp	;;11 numero de disparos	 
	nombre'_colS: 	.db #0        		;;12 comprueba si colisona contra el suelo (0-no,1-si)
	nombre'_nSaltos: .db #0			;;13 comprueba el doble salto
	nombre'_id:     .db #1         		;;14 - Identificador de enemigo
	nombre'_pdirx:   .db #0         	;;15 (0-PJizquierda, 1-PJderecha)
	nombre'_pdiry:   .db #0      		;;16 (0- PJarriba, 1-PJabajo)
	nombre'_psame:   .db #0        		;;17 (misma posicion en el eje y) (0-no,1-si)
	nombre'_pvision: .db #0         	;;18 he visto al enemigo 1, no lo veo 0
	nombre'_eshot:   .db #0         	;;19 el enemigo se encuentra disparando 1, no dispara 0
	nombre'_dmov:    .db #-1         	;;20 numeor random para saber donde me tengo que mover
	nombre'_contd:   .db contdirection	;;21 contador para cambiar la direccion del jugador
	nombre'_platup:  .db #0          	;;22 ARRIBA(0-No encuentro plataforma, 1-Si encuentro plataforma)
	nombre'_platdia: .db #0          	;;23 DIAGONAL(0-No encuentro plataforma, 1- Si encuentro plataforma)
	nombre'_cntrolmap: .db #0        	;;24 (0 si ha llegado al lado izquierdo, 1 si ha llegado al lado derecho)
	nombre'_contvision: .db contvision 	;;25
	nombre'_activo:     .db #1		;;26 variable para saber si esta activado o no
	nombre'_contEnemis: .db #3       	;;27 variable para saber cuantos enemigos debo recorrer (crear constante)
	nombre'_semillaXor:.db semillaXor      	;;28 plusSeed tiene que ser un numero impar
	nombre'_rsalto:   .db #0		;;29 numero aleatorio para el salto

.endm	


;;------------------------------BALA-----------------------------------------------
.macro bala nombre, pos_x, pos_y, ancho, alto, direccion, sprite

nombre'_data:
	
	nombre'_x: 	.db pos_x	;;0
	nombre'_y: 	.db pos_y	;;1
	nombre'_w: 	.db ancho	;;2
	nombre'_h: 	.db alto	;;3
	nombre'_dir:	.db direccion	;;4  (0- derecha, 1-izquierda)
	nombre'_spr: 	.dw sprite 	;;5-6
	nombre'_activo: .db #0 		;;7 (0 - descativado, 1 - activado)
	nombre'_numbalas: .db#10        ;;8 (30 balas)
	nombre'_px: 	.db pos_x       ;;9 posicion previa para borrar X
	nombre'_py:	.db pos_y	;;10 posicion previa para borrar y
	nombre'_id: .db #0 ;; 11 - Identificador del creador de la bala

.endm	

;; -----------------------------------------
;; CONSTANTES
;; -----------------------------------------
;;----------------COMUNES----------------------------------
.equ Ent_x,0	
.equ Ent_y,1
.equ Ent_w,2
.equ Ent_h,3
.equ Ent_dir,4
.equ Ent_spr_l,5
.equ Ent_spr_h,6
.equ Ent_vida,7
.equ Ent_px,9
.equ Ent_py,10

.equ Ent_ndisp,11
.equ Ent_colS,12
.equ Ent_nSaltos,13
.equ Ent_id,14

;;------------------------------Enemigo----------------------
.equ Ent_pdirx,15
.equ Ent_pdiry,16
.equ Ent_psame,17
.equ Ent_pvision,18
.equ Ent_eshot,19
.equ Ent_dmov,20
.equ Ent_contd,21
.equ Ent_platup,22
.equ Ent_platdia,23
.equ Ent_cntrolmap,24
.equ Ent_contvision,25
.equ Ent_activo,26
.equ Ent_contEnemis,27	
.equ Ent_semillaXor,28
.equ Ent_rsalto,29

;;  BALA -------------------
.equ bala_id, 11 ;; Identificador del creador de la bala