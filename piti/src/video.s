.area _DATA

.area _CODE
	
.include "cpctelera.h.s"


videoPointer: .dw #0x8000

video_cambiarBuffers::


	modifier = .+1
	ld l, #0x20
 	call cpct_setVideoMemoryPage_asm		
 	ld hl,#modifier
 	ld a,#0x10
 	xor (hl)
 	ld (modifier),a

 	ld hl, #videoPointer+1
 	ld a,#0x40
 	xor (hl)
 	ld (videoPointer+1),a


 ret


 video_getVideoPointer::

 	ld hl,(videoPointer)

  ret