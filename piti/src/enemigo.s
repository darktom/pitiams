.area _DATA

.area _CODE
.include "cpctelera.h.s"
.include "macros.h.s"
.include "entity.h.s"
.include "video.h.s"  
.include "bala.h.s"
.include "map.h.s"
.include "hero.h.s"

.globl _sprite_Enemy_0


.globl _sprite_Enemy_1


enemigos_activos: .db #3  

;; -----------------------------------------
;; -----------------------------------------
;; PRIVATE DATA
;; -----------------------------------------
;; -----------------------------------------

.equ En_vision_x,20 ;; rnago de vision en x
.equ En_vision_y,50 ;; rango de vision en y
.equ En_contvision,60 ;; tiempo para que se reinicie la busqueda
.equ En_coldowndisp,30 ;; coldown del disparo
.equ En_shotrange,30 ;; rango de disparo eje x
.equ En_contdirection,10 ;; frecuencia con la que cambia de direccion 
.equ En_distplatarriba,5 ;; rango de vision de plataforma arriba
.equ En_distaplatdiagonal,6;; rango de vision de plataforma diagonal
.equ En_dist_platafdiagonalRL,5;;rango de vision de plataforma diagonal derecha izquierda

aleatoriod: .db #40
aleatorioe: .db #20
aleatorioh: .db #60
aleatoriol: .db #100    


Posx_1:     .db #60
Posy_1:     .db #90
Vidas_1:    .db #3
Posx_2:     .db #30
Posy_2:     .db #50
Vidas_2:    .db #3
Posx_3:     .db #10
Posy_3:     .db #30
Vidas_3:    .db #3
;;Posx_4:     .db #60
;;Posy_4:     .db #90
;;Vidas_4:    .db #3



jumptable:
    .db #-20, #-5, #-5, #-3
    .db #-3, #-2, #-2, #-1
    .db #-1, #0x80 ;; Finish jumping flag



enemigo enemi, 60, 90, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 1
enemigo enemi2, 30, 50, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 2
enemigo enemi3, 10, 30, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection,1
enemigo enemi4, 31, 13, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 2
;;enemigo enemi5, 3, 31, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 45
;;enemigo enemi6, 13, 3, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 63
;;enemigo enemi7, 52, 13, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 24
;;enemigo enemi8, 13, 23, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 6
;;enemigo enemi9, 45, 13, 8, 18, 3, 0, _sprite_Enemy_0,#En_contvision,#En_coldowndisp,#En_contdirection, 36




;; -----------------------------------------
;; ARRAY DE SALTO NPCS
;; -----------------------------------------

;; -----------------------------------------
;; -----------------------------------------
;; PUBLIC FUNCTIONS
;; -----------------------------------------
;; -----------------------------------------

init_enemis::


    ld a,#3
    ld (enemigos_activos),a

    ld ix, #enemi_data

    ld a,(Posx_1)
    ld Ent_x(ix),a
    ld a,(Posy_1)
    ld Ent_y(ix),a
    ld a,(Vidas_1)
    ld Ent_vida(ix),a
    ld a,#1
    ld Ent_activo(ix),a
    ld bc,#0x001E
    add ix,bc
    

    ld a,(Posx_2)
    ld Ent_x(ix),a
    ld a,(Posy_2)
    ld Ent_y(ix),a
    ld a,(Vidas_2)
    ld Ent_vida(ix),a
    ld a,#1
    ld Ent_activo(ix),a
    ld bc,#0x001E
    add ix,bc

    ld a,(Posx_3)
    ld Ent_x(ix),a
    ld a,(Posy_3)
    ld Ent_y(ix),a
    ld a,(Vidas_3)
    ld Ent_vida(ix),a
    ld a,#1
    ld Ent_activo(ix),a

    ret

;; -----------------------------------------
;; devuelve el numero de enemigos activos
;; output:A
;; -----------------------------------------
enemigo_Getnumactivo::

    ld a, (enemigos_activos)

    ret

;;-------------------------------------------------
;;Restar numero de enemigos
;;-----------------------------------------------
enemigo_Decactivos:

    ld a, (enemigos_activos)
    cp #0
    jr z, no_resto
        dec a
        ld (enemigos_activos),a
    no_resto:

    ret
;;-----------------------------------------------------------------
;; resta vida al enemigo y en el caso de que no le quede mas lo borra
;;INPUTS:IX
;;-----------------------------------------------------------------
enemigo_hit::
    ;; IF life > 0 => life--
    
    ld a, Ent_vida(ix)      ;; A = Vida del enemigo
    cp #1               ;; Comprobar si es > 0
    jr z, Erase_enemi     ;; Caso borrar enemigo

        ;; Aun le queda vida
        dec a           ;; Se le resta vida
        ld Ent_vida(ix), a  ;; Actualizamos variable
        
        ret             ;; Volvemos al ruedo

    Erase_enemi:
        
        ;; si no le queda vida lo borramos y lo desactivamos, para no actualizarlo    
        call enemigo_erase
        
        ld a,#0
        ld Ent_activo(ix),a

        call enemigo_Decactivos

    ret

;; -----------------------------------------
;; Draws the obstacle
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
enemigo_drawAll::

    ld ix, #enemi_data

    ld a, Ent_contEnemis(ix)
    ld e,a

    bucle_draw:

    push de
    ld a,Ent_activo(ix)
    cp #0
    jr z, no_esta_activodraw

    call video_getVideoPointer
    ex de,hl
    call draw


    no_esta_activodraw:

    pop de 
    dec e
    ld bc,#0x001E
    add ix,bc
    jr nz,bucle_draw



    ret

;; -----------------------------------------
;; Erases the obstacle
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
enemigo_eraseAll::


    ld ix, #enemi_data
    ld a, Ent_contEnemis(ix)
    ld e,a

    bucle_erase:

    push de
    ld a,Ent_activo(ix)
    cp #0
    jr z, no_esta_activoerase 
    
    call video_getVideoPointer
    ex de,hl
    call erase_sprite

    no_esta_activoerase:

    pop de 
    dec e
    ld bc,#0x001E
    add ix,bc
    jr nz,bucle_erase


    ret


;; -----------------------------------------
;; Erases the obstacle
;; Input:IX
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
enemigo_erase::

          
    call video_getVideoPointer
    ex de,hl
    call erase_sprite
    
    call video_cambiarBuffers

    call video_getVideoPointer
    ex de,hl
    call erase_sprite
    
    call video_cambiarBuffers    

    ret

;; -----------------------------------------
;; Update the obstacle
;; Inputs : IX
;; DESTROYS: AF, BC, DE, HL
;; -----------------------------------------
enemigo_update:
    ;; Move obstacle to the left

    ;; actializamos la posicion anterior para el borrado del tilemap
        
    ld a, Ent_x(ix)
    ld Ent_px(ix),a
    ld a,Ent_y(ix)
    ld Ent_py(ix),a 


    
    call enemigo_vision   
    call pos_pj_x_y
    call pos_pj_y_same
    call enemigo_mov
    call jumpControl
    call enemigo_fall
    call comprobar_sprite_dir

    ;;call bala_GetPointerData
    ;;call enemigo_esquiva

    ret



enemigo_Update::

    ld ix, #enemi_data

    ld a, Ent_contEnemis(ix)
    ld e,a

    bucle_update:

    push de
    ld a,Ent_activo(ix)
    cp #0
    jr z, no_esta_activoupdate 

    
    call map_getPointerMap
    call hero_GetPointerDataIY
    call enemigo_update

    call map_getPointerMap
    call collision_map

    call bala_GetPointerData
    call collision

    no_esta_activoupdate:

    pop de 
    dec e
    ld bc,#0x001E
    add ix,bc
    jr nz,bucle_update


    ret

;;vision del enemigo

enemigo_vision:
    ;; Rango de vision del enemigo

    

    ld a,Ent_contvision(ix)
    cp #En_contvision
    jr nz, no_reinicio_busqueda
    ld a,#0
    ld Ent_pvision(ix),a
    ld Ent_contvision(ix),a
    ;;ld a, #0x00
    ;;ld (0xC002),a
    ;;ld (0xC003),a

    ret 

    no_reinicio_busqueda:

    ld a,Ent_contvision(ix)
    inc a
    ld Ent_contvision(ix),a

    ;; primero comprobamos si el personaje se encuentra a la izquierda o a la derecha 
    ld a, Ent_pdirx(ix)
    cp #0
    jr nz, esta_derecha
    ;;esta en la izquierda

    ;; comprobamos que se encuentra a la distancia en x donde puede verlo
    ;;hero_x - enemigo_x + 60 >= 0
    ld a, Ent_x(ix) 
    ld b,#En_vision_x ;;distancia limite de vision
    sub b
    ld b,a
    ld a, Ent_x(iy) ;;---------IY-------
    sub b
    
    jr z, no_me_vei
    jp m, no_me_vei

    ;; me ve por la izquierda
    jr continuo
    
    no_me_vei:

    ret

    esta_derecha:

    ld a, Ent_x(ix)
    ld b,#En_vision_x
    add b
    ld b,a

    ld a, Ent_x(iy) ;;---------IY-------

    ;;hero_x - enemigo_x - 30 <= 0

    sub b
    
    jr z, no_me_ved
    jp p, no_me_ved

    ;; me ve a la derecha

    jr continuo

    no_me_ved:
    ;; condiciond e que no me ve

    
    ret 

    continuo:

    ;; primero comprobamos si el personaje se encuentra arriba o abajo 
    ld a, Ent_pdiry(ix)
    cp #0
    jr nz, esta_abajo

    ;;hero_y + 30 - enemigo_y >=0
    
    ld a, Ent_y(ix) ;; enemigo_y
    ld b,a
    ld a, Ent_y(iy) ;;hero_y
    ld c,#En_vision_y
    add c
    sub b

    jr z, no_me_ar
    jp m, no_me_ar

    ;; me estas viendo
    ;;ld a, #0xFF
    ;;ld (0xC002),a
    ;;ld (0xC003),a

    ld a,#1
    ld Ent_pvision(ix),a
    ld a,#0
    ld Ent_contvision(ix),a

    ret 

    no_me_ar:

    ret

    esta_abajo:

    ;;enemigo_y - 30 - hero_y <=0 
  
    ld a, Ent_y(iy) ;;---------IY-------
    ld b,a
    ld a, Ent_y(ix)
    sub b
    ld b,#En_vision_y
    add b

    jr z, no_me_ab
    jp m, no_me_ab

    ;;ld a, #0xFF
    ;;ld (0xC002),a
    ;;ld (0xC003),a

    ld a,#1
    ld Ent_pvision(ix),a

    ld a,#0
    ld Ent_contvision(ix),a

    ret 

    no_me_ab:
  

    ret


enemigo_mov:


    ld a, Ent_pvision(ix)
    cp #0
    jr nz, veo_al_pj

    ;; rutina de movimiento enemigo  aleatoria cuando no ve al jugador
    

    call enemigo_jump_plataforma
    

    ld a, Ent_contd(ix)
    cp #0
    jr nz, no_cambio_direccion

    ld a,#En_contdirection
    ld Ent_contd(ix),a

    call numero_aleatorio
    ld Ent_dmov(ix), a
    ret

    no_cambio_direccion:

    dec a
    ld Ent_contd(ix),a

    ld a,Ent_dmov(ix)
    cp #0

    jp m, es_negativo_derecha

    call moveEnemiLeft

    ret 

    es_negativo_derecha:

    call moveEnemiRight
   

    ret  

    veo_al_pj:  

    ;; rutina de movimiento enemigo cuando ve al jugador

    ;; llamo a disparar , si disparo genial si no tengo que buscar al jugador para dispararle

    call enemigo_shot
    ld a,Ent_eshot(ix)
    cp #0
    jr nz, disparo_j

    ;;si el enemigo se encuentra en la misma posicion en el eje y solo tengo que ir a por el 
    ld a,Ent_psame(ix)
    cp #0
    jr nz, misma_posicion_y
    ;; no estoy en la misma posicion en el eje y, por lo tanto miro si esta arriba o abajo
    ld a,Ent_pdiry(ix)
    cp #0
    jr nz, esta_abajop
    ;; se encuentra arriba
    
    call controlmap
    ld a,Ent_cntrolmap(ix)
    cp #0
    jr nz, busco_izquierda

    call moveEnemiRight
    call enemigo_jump_plataforma
    
    
    ret

    busco_izquierda:
    call moveEnemiLeft
    call enemigo_jump_plataforma

    ret

    esta_abajop:

;;ld a,#0
;;ld (enemi_colS),a COMO ESTO NO ME HACE NI PUTO CASO PONGO OTRA COSA XD
    
;; no me he complicado mucho la vida xDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD

    call controlmap
    ld a,Ent_cntrolmap(ix)
    cp #0
    jr nz, busco_izquierdaa

    call moveEnemiRight
    ret

    busco_izquierdaa:
    call moveEnemiLeft

    ret

    misma_posicion_y:
    ;; si estoy en la misma posicion en y por lo tanto miro si esta a derecha o izquierda

    call numero_aleatorio
    ld (Ent_dmov),a

    ld a,Ent_pdirx(ix)
    cp #0
    jr nz, esta_en_la_derecha
    ;; izquierda

     ld a,(Ent_dmov)
     cp #0
     jp m, no_voy_por_el 
    call moveEnemiLeft
    ;;call plataforma_diagonal


    ret 
    esta_en_la_derecha:
    ;; derecha
    ld a,(Ent_dmov)
    cp #0
    jp m, no_voy_por_el

    call moveEnemiRight
    ;;call plataforma_diagonal

    no_voy_por_el:
    disparo_j:     


    ret

numero_aleatorio:

    ld a, 15(iy)
    cp #0
    jr nz, no_me_muevo

    ld a, (aleatoriod)
    inc a
    ld d,a
    ld (aleatoriod),a

    ld a, (aleatorioe)
    inc a
    ld e,a
    ld (aleatorioe),a

    ld a, (aleatorioh)
    inc a
    ld h,a
    ld (aleatorioh),a

    ld a, (aleatoriol)
    inc a
    ld l,a
    ld (aleatoriol),a

    jr continuoaleatorio

    no_me_muevo:

    ld d, Ent_x(iy)
    ld e, Ent_y(iy)
    ld h, Ent_y(iy)
    ld l, Ent_x(iy)


    continuoaleatorio:

    ld a, Ent_semillaXor(ix)
    call cpct_setSeed_xsp40_u8_asm
    call cpct_getRandom_xsp40_u8_asm ;; en a y en l tendo el valor del numero aleatorio de 8 bits
    


    ret




controlmap:

    ld a,Ent_px(ix)
    cp #1
    jr nz, miro_final_derecha

    ld a,#0
    ld Ent_cntrolmap(ix),a

    ret

    miro_final_derecha:

    ld a,Ent_px(ix)
    cp #71
    jr nz, no_miro_mas

    ld a,#1
    ld Ent_cntrolmap(ix),a

    no_miro_mas:

    ret 



;; compruebo a que lado se encuentra el personaje 
pos_pj_x_y:

    ld a,Ent_x(iy) ;;---------IY-------
    ld b,a
    ld a, Ent_x(ix)
    sub b

    jp m, pj_derecha
    ld a,#0
    ld Ent_pdirx(ix),a
    
    jr siguiente

    pj_derecha:
    ld a,#1
    ld Ent_pdirx(ix),a
    
    siguiente:

    ld a,Ent_y(iy) ;;---------ix-------
    ld b,a
    ld a,Ent_y(ix)
    sub b

    jp m, pj_abajo

    ld a,#0
    ld Ent_pdiry(ix),a

    ret
    pj_abajo:

    ld a,#1
    ld Ent_pdiry(ix),a

    ret


pos_pj_y_same:

    ;; comprobamos si el personaje se encuentra en el mismo eje y 

    ;; hero_y + hero_h <= enemi_y
        ld a , Ent_y(ix)
        ld b,a         
        ld a , Ent_y(iy) ;;---------IY-------
        ld c , a       
        ld a , Ent_h(ix) ;;---------IY------- 
        add c           
        sub b           
        jr z, no_esta_en_y
        jp m, no_esta_en_y

        
    ;;enemi_y + enemi_h <= hero_y

        ld a, Ent_y(iy)  ;;---------IY-------
        ld c,a
        ld a, Ent_h(ix)
        add b
        sub c
        jr z, no_esta_en_y
        jp m, no_esta_en_y
            
        ld a, #1
        ld Ent_psame(ix),a

        ret 

        no_esta_en_y:

        ld a, #0
        ld Ent_psame(ix),a


    ret



enemigo_fall:

    ;; si el enemigo no colisiona tiene que caer abajo

    ld a, Ent_colS(ix)
    cp #1
    jr nz, no_colisiona

    ld a,#1
    ld Ent_nSaltos(ix),a

    ret 

    no_colisiona:

    ld a,#0
    ld Ent_nSaltos(ix),a

    ld a, Ent_y(ix)
    inc a
    inc a
    ld Ent_y(ix),a


    ret

enemigo_GetPointerDataIX::
    
    ld ix ,#enemi_data

    ret


enemigo_GetPointerDataIY::

    ld iy ,#enemi_data

    ret     


enemigo_shot:
   
    ld a,#0
    ld Ent_eshot(ix),a

    ;; comprobamos que esta en la misma posicion en y
    ld a,Ent_psame(ix)
    cp #0
    jr z, no_disparo

    ;; primero comprobamos si el personaje se encuentra a la izquierda o a la derecha 
    ld a, Ent_pdirx(ix)
    cp #0
    jr nz, disparo_derecha 

    ;; comprobamos que se encuentra a la distancia en x donde puede verlo
    ;;hero_x - enemigo_x + 60 >= 0
    ld a, Ent_x(ix) 
    ld b,#En_shotrange ;;distancia limite de vision
    sub b
    ld b,a
    ld a, Ent_x(iy) ;;---------IY-------
    sub b
    
    jr z, no_dispara
    jp m, no_dispara

    ld a,Ent_ndisp(ix) ;; contador para que el enemigo no dispare constantemente
    cp #En_coldowndisp 
    jr nz, no_disparo_aun

   

    ;;ld ix, ix ;; -----------------------------------------------------------------------
    
    ld a,#1
    ld Ent_dir(ix),a
    ld b, #1    ;; 0 => Bala del enemigo
    call bala_buscarBala

    ;;call comprobar_sprite_dir  

    ld a,#1
    ld Ent_eshot(ix),a ;; el enemigo dispara finalmente

    ld a,#0 ;;una vez dispare pongo el contador a 0 para restablecer la cuenta
    ld Ent_ndisp(ix),a     
    
    ret 

    no_disparo_aun:

    inc a ;; si no disparo aun incremento el contador
    ld Ent_ndisp(ix),a
    
    ret     
    
    no_dispara:
    
    ld a,#En_coldowndisp  ;; si el personaje no te ve reestablece el disparo, para poder disparar instantaneamente cuando te vuelva a ver
    ld Ent_ndisp(ix),a

    ret 

    disparo_derecha:


    ld a, Ent_x(ix)
    ld b,#En_shotrange
    add b
    ld b,a

    ld a, Ent_x(iy) ;;---------ix-------

    ;;hero_x - enemigo_x - 30 <= 0

    sub b
    
    jr z, no_dispara_d
    jp p, no_dispara_d

    ld a,Ent_ndisp(ix)
    cp #En_coldowndisp
    jr nz, no_disparo_aun_d

    

    ;;ld ix, ix
    ld a,#0
    ld Ent_dir(ix),a
    ld b, #1    ;; 0 => Bala del enemigo
    call bala_buscarBala

    ;;call comprobar_sprite_dir
 
    ld a,#1
    ld Ent_eshot(ix),a

    ld a,#0
    ld Ent_ndisp(ix),a     
    
    ret 

    no_disparo_aun_d:

    inc a
    ld Ent_ndisp(ix),a
    ret     
    no_dispara_d:
    
    ld a,#En_coldowndisp
    ld Ent_ndisp(ix),a

    no_disparo:

    ret        





moveEnemiLeft:

    

    ;;call comprobar_sprite_dir
    ld a,#0
    ld Ent_dir(ix),a

    ld a, Ent_x(ix)              ;; A = Hero_x
    cp #1                       ;; Check left limit
    jr z, not_move_l            ;; Hero_x == limit, do not move left
    
        ;; Move left
        dec a 
        ;;dec a                   ;; A-- (Hero_x--)
        ld Ent_x(ix), a          ;; Update Hero_x from A
    
    not_move_l:
    


    ret


;; -----------------------------------------
;; Move hero right if it is not at the limit
;; DESTROYS: AF
;; -----------------------------------------
moveEnemiRight:


    ;;call comprobar_sprite_dir
    
    ld a,#1
    ld Ent_dir(ix),a
    ld a, Ent_x(ix)              ;; A = Hero_x
    cp #71                    ;; Check right limit
    jr z, not_move_r            ;; Hero_x== limit, do not move right
    
        ;; Move right
        inc a
        ;;inc a                       ;; A++ (Hero_x++)
        ld Ent_x(ix), a              ;; Update Hero_x from A
    
    not_move_r:
    
    ret




enemigo_jump_plataforma:

    call plataforma_diagonal
    
    ret 

jumpControl:

    ;; Check if we are jumping right now
    ld a, 8(ix)       ;; A = Hero_jump status
    cp #-1                  ;; A == -1? (-1 is not jumping)
    ret z                   ;; If A==-1, not jumping, return
    ;;ld hl, #_sprt_player_3
    ;;ld Ent_spr_l(ix), l
    ;;ld Ent_spr_h(ix), h
    ;; Get jump value
    ld hl, #jumptable       ;; HL Point to start of the jumping table
    ld c, a                 ;; |
    ld b, #0                ;; \ BC = A (offset)
    add hl, bc              ;; HL += BC

    ;; Check end of jumping
    ld a, (hl)              ;; A = Jump movement
    cp #0x80                ;; Jump value == 0x80? End of jumping
    jr z, end_of_jump       ;; If 0x80, end of jump

    ;; Do jump movement
    ld b, a                 ;; B = A = Jump movement
    ld a, Ent_y(ix)          ;; A = Hero_y
    add b                   ;; A += B (Add jump movement)
    ld Ent_y(ix), a          ;; Update hero_y value
    ;;ld hl, #_sprt_player_3                  ;;|
    ;;ld Ent_spr_l(ix), l
    ;;ld Ent_spr_h(ix), h                   ;;\cuando no detecta colision , se activa el sprite del salto
    ;; Increment hero_jump index
    ld a, 8(ix)       ;; A = Hero_jump
    inc a                   ;; |
    ld 8(ix), a       ;; \ Hero jump ++
   

    ret

    ;; Put -1 in the jump index when jump ends
    end_of_jump:
        ld a, #-1           ;; |
        ld 8(ix), a   ;; \ Hero_jump = -1
        ;;ld hl, #_sprt_player_0
        ;;ld Ent_spr_l(ix), l
        ;;ld Ent_spr_h(ix), h
      
       
    ret
   
;; -----------------------------------------
;; Starts hero jump
;; DESTROYS: AF
;; -----------------------------------------
startJump:

    ld a, Ent_nSaltos(ix) 
    cp #0
    jr z,no_salto
    
    ld a,#0
    ld Ent_nSaltos(ix),a
    ld a, 8(ix)       ;; A = Hero_jump
    cp #-1                  ;; A == -1? Is jump active?  
    ret nz                  ;; Jump active, return

    ;; Jump is inactive, activate it
    ld a, #0
    ld 8(ix), a
 
    no_salto:

    
    ret    


plataforma_diagonal:

    ld a,#0
    ld Ent_platdia(ix),a

    ;;ld a,#0x00
    ;;ld (0xC000), a
    ;;ld (0xC100), a
        
;; diagonal arriba
;; ajustar salto diagonal al mapa    
    call posicion_tilemap ;; c=x , b=y 
    call tiles_ancho_alto ;; e=w , d=h 

    ld a,d
    add b
    ld b,a

    ld e,#En_distaplatdiagonal

    bucled:

    ld a,b
    sub e
    push bc
    ld b,a

    ld a,Ent_dir(ix)
    cp #0
    jr nz, miro_derecha

    ld a,c
    ld d,#En_dist_platafdiagonalRL
    sub d ;; condicion para que sume o reste diagonal izquierda u diagonal derecha
    ld c,a

    jr salto_porq_me_sale_del_orto


    miro_derecha:

    ld a,c
    ld d,#En_dist_platafdiagonalRL
    add d ;; condicion para que sume o reste diagonal izquierda u diagonal derecha
    ld c,a

    salto_porq_me_sale_del_orto:

    push de

    ld de,#40 ;;cargamos el valor 40 que es lo que ocupa 1 fila de ancho en el array del mapa
    push hl
    call multiplicar
    add hl,bc

    ld a, (hl)
    cp #0
    jr nz,no_salto_diagonal

    
;;    call numero_aleatorio
;;    ld Ent_rsalto(ix),a
;;
;;    ld a,Ent_rsalto(ix)
;;    cp #0
;;
;;    jp m, no_saltoo

    call startJump
    
;;   no_saltoo:
    ;;ld a,#1
    ;;ld Ent_platdia(ix),a
   
    pop hl
    pop de
    pop bc

    ret 

    no_salto_diagonal:

    ; ;call startJump
    pop hl
    pop de
    pop bc
        
    dec e
    jr nz,bucled


;;diagonal_al_lado

    ret


;;------------------------
;;Inputs: IY
;;------------------------
;;enemigo_esquiva:
;;
;;
;;
;;    ld c, 8(iy)        ;; cargamos el numero total de balas para recorrer el bucle
;;    
;;    loop_esquiva:
;;    
;;    ld a, bala_id(iy)
;;    cp #0
;;    jp z, no_hago_nadaa
;;    
;;    ld 7(ix),a        ;; cargamos si la bala esta activada (1-activada,0-desactivada)
;;    cp #0
;;    jp z, no_hago_nadaa
;;    
;;      ;; enemi_y + enemi_h <= bala_y
;;        ld a , Ent_y(ix)
;;        ld b,a         
;;        ld a , Ent_y(iy) ;;---------IY-------
;;        ld c , a       
;;        ld a , Ent_h(ix) ;;---------IY------- 
;;        add c           
;;        sub b           
;;        jp z, no_hago_nadaa
;;        jp m, no_hago_nadaa
;;
;;        
;;    ;;bala_y + bala_h <= enemi_y
;;
;;        ld a, Ent_y(iy)  ;;---------IY-------
;;        ld c,a
;;        ld a, Ent_h(ix)
;;        add b
;;        sub c
;;        jp z, no_hago_nadaa
;;        jp m, no_hago_nadaa
;;            
;;        ld a, Ent_x(ix)
;;        ld b,a
;;        ld a,Ent_x(iy)
;;        sub b
;;        jp m ,viene_izquierda
;;        ;;derecha
;;
;;        ld a, Ent_x(ix)
;;        ld b,#En_shotrange
;;        add b
;;        ld b,a
;;        ld a, Ent_x(iy) ;;---------ix-------
;;        sub b
;;    
;;        jp z, no_hago_nadaa
;;        jp p, no_hago_nadaa
;;
;;        call startJump
;;
;;        jp no_hago_nada
;;
;;        viene_izquierda:
;;
;;        ld a, Ent_x(ix) 
;;        ld b,#En_shotrange ;;distancia limite de vision
;;        sub b
;;        ld b,a
;;        ld a, Ent_x(iy) ;;---------IY-------
;;        sub b
;;    
;;        jp z, no_hago_nadaa
;;        jp m, no_hago_nadaa
;;
;;
;;        call startJump
;;
;;        ret 
;;
;;    no_hago_dadaa:
;;
;;    ld de,#0x000C
;;    add ix,de      
;;    dec c 
;;    jr nz, loop_esquiva
;;
;;    ret 



;;plataforma_arriba:
;;
;;    ld a,#0
;;    ld Ent_platup(ix),a
;;    
;;    call posicion_tilemap ;; c=x , b=y 
;;
;;    ld e,#En_distplatarriba
;;
;;    buclea:
;;
;;    ld a,b
;;    sub e
;;    push bc
;;    ld b,a
;;
;;    push de
;;
;;    ld de,#40 ;;cargamos el valor 40 que es lo que ocupa 1 fila de ancho en el array del mapa
;;    push hl
;;    call multiplicar
;;    
;;
;;    add hl,bc
;;    ld a, (hl)
;;    cp #0
;;    jr nz,no_salto_arriba
;;
;;
;;    ;;call startJump
;;
;;    ld a,#1
;;    ld Ent_platup(ix),a
;;
;;    
;;    ;; veo una plataforma en mi cabolo a la que puedo llegar
;;
;;    pop hl
;;    pop de
;;    pop bc
;;
;;    ret 
;;
;;    no_salto_arriba:
;;
;;    pop hl
;;    pop de
;;    pop bc
;;        
;;    dec e
;;    jr nz,buclea
;;
;;
;;    ret 
;;




;;====================================================
;;comprobar_sprite_dir
;; INPUT HL: Sprite_0 DE:Sprite_1 , B: direction
;;====================================================
comprobar_sprite_dir:

    ld a, Ent_dir(ix)         ;; comprobar si es 0 , der , 1 izq
    cp #0
    jr nz, mirarDer

        ;; mirar derecha
        ld hl, #_sprite_Enemy_1
        ld Ent_spr_h(ix),  h        ;; en hl el sprite0 
        ld Ent_spr_l(ix) , l
        jr comprobar_sprite_fin

       

    mirarDer:

        ld hl, #_sprite_Enemy_0
        ld Ent_spr_h(ix),  h        ;; en hl el sprite0 
        ld Ent_spr_l(ix) , l
       
    comprobar_sprite_fin:

    ret

        